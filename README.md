

##### _May 02, 2021_ 

**MSContests** is a library for displaying and participating to contests.

![kotlin_version 1.4.32](https://img.shields.io/badge/Kotlin-1.4.3-blue.svg)
![Min SDK 21](https://img.shields.io/badge/Min_SDK-21-green.svg)
![Target SDK 30](https://img.shields.io/badge/Target_SDK-30-green.svg)


# Quick Start Guide

## Installation

### Dependencies in *app/build.gradle*

Add the following lines to the `dependencies` section in *app/build.gradle*

```gradle
implementation "co.madseven.sdk:mscontest:0.0.18"
```

Add the following lines to the allProject section in *build.gradle*

```gradle
allprojects {
    repositories {
        maven {
            url 'https://gitlab.com/api/v4/projects/26454181/packages/maven'
        }
    }
}
```

### Permissions in *app/src/main/AndroidManifest.xml*

```xml
<!-- This permission is required to allow the application to send events and properties to MSContest and to request contest information -->
<uses-permission android:name="android.permission.INTERNET" />
```

## Integration

### Initialization

Initialize MSContest in your application *app/src/main/java/co/madseven/contest/demo/App.kt*. Usually this should be done in [onCreate](https://developer.android.com/reference/android/app/Application#onCreate()).


```kotlin
ContestSDK.Builder(context, null)
            .apiToken("YOUR_PROJECT_TOKEN")
            .clientId("YOUR_APP_ID")
            .debugMode(true) //false by default
            .build()
```

Remember to replace `YOUR_PROJECT_TOKEN` with the token provided to you on mscontest.
Remember to replace `YOUR_APP_ID ` with the package name of your application

Add .debugMode to have some logs.

### Customisation

To participate user will need to give username and mail to be informed when he wins. You have two options:
		<ul> Use the authentication integrated by mscontest, nothing to </ul>
		<ul> Use your own authentication system. </ul>
		
#### Custom Authentication system

To use your own authentication system, you must declare two param with ContestSDK.Builder :

```kotlin
val callback = object : UserNotConnectedCallback {
            override fun onUserNotConnected(activity: Activity) {
                val loginDialog = LoginDialog.newInstance()
                loginDialog.start((activity as AppCompatActivity).supportFragmentManager)
            }
        }

ContestSDK.Builder(this, this)
            .apiToken("YOUR_PROJECT_TOKEN")
            .clientId("YOUR_APP_ID")
            .linkAppUser(true)
            .userNotConnectedCallback(callback)
            .build()
```

Set linkAppUser to true,if not the mscontest authenticator will be triggered instead.
Set userNotConnectedCallback,like this mscontest will trigger this callback to allow you to start your authentification workflow.
 
If you already have all the user informations, you can call this method when sdk is proprely initialized to prevent displaying authentification prompt :

```kotlin
ContestSDK.linkUser(userName, userMail,userId)
```

userId is not mandatory but it will be used to improved tracking

After your authentication is done DO NOT FORGET to call ContestSDK.linkUser

#### UI Customization

The last param you can give to ContestSDK.Builder is a ContestThemeConfig. This object will allows to custom some part of the UI.

```kotlin
val contestThemeConfig =
            ContestThemeConfig.Builder()
                .mainColor(R.color.appMainColor)
                .backgroundColor(R.color.appBackgroundColor)
                .canParticipateFromList(false)
                .displayTimerFromDay(2)
                .timerIcon(R.drawable.ic_baseline_av_timer_24)
                .playerIcon(R.drawable.ic_round_play_circle_24)
                .mediumFontRes(R.font.montserrat_medium)
                .regularFontRes(R.font.montserrat_regular)
                .lightFontRes(R.font.montserrat_light)
                .showContestSection(false)
                .build()

ContestSDK.Builder(this, this)
            .apiToken("YOUR_PROJECT_TOKEN")
            .clientId("YOUR_APP_ID")
            .themeConfig(contestThemeConfig)
            .build()
```

All param are optionnal, with a default value.

| Params      | Description | Default |
| ----------- | ----------- | ----------- |
| primaryColor   | colorResId Modify several component to apply the color of your choice.| #027AFF|
| backgroundColor| colorResId Modify all background of all activities handled by mscontest.|#FFFFFF|
| textColor| colorResId Modify text color in contest detail.|#3C3C3C|
| mediumFontRes | fontResId Use by some texts.| roboto_medium |
| regularFontRes | fontResId Use by some texts.| roboto_regular |
| lightFontRes | fontResId Use by some texts.| roboto_light |
| canParticipateFromList | Display the participate button in the list of contests.|true|
| displayTimerFromDay | Number of day reminding before contest end to display a countdown timer.|2|
| shouldDisplaySponsorInList | Show sponsor in contest list| true |
| appLogo | drawableResId Your App logo will be display when asking more user infos| -1 |


### Display Contest List

When the sdk is init you can call these method to display ContestListActivity when needed:


```kotlin
ContestSDK.showContestListScreen(context)
```

#### As fragment

To get an instance of ContestListFragment, that you can add anywhere :

```kotlin
ContestListFragment.newInstance()
```

### Push Notification

You can send the user push token to MSContest. With the push token we can send push to display new contest or to tell a user when he win.

To link a push token call :

```kotlin
ContestSDK.linkPushToken(USER_PUSH_TOKEN)
```

Remember to call it each time the push token is updated.

When we send you a push, your push receiver will be triggered et you will have to handle the way you want to display it.

Here the structure of the push sent :

```json
{
 "to" : "USER_PUSH_TOKEN",
 "collapse_key" : "type_a",
 "data" : {
     "channel" : "Alerte Concours",
     "title": "Maillot de l'équipe de France",
     "body" : "Un nouveau maillot à gagner, fonce participer !!!"
 }
}
```

For example with FirebaseMessagingService you can retreive these value like this :


```kotlin
val channel = remoteMessage.data["channel"]
val title = remoteMessage.data["title"]
val body = remoteMessage.data["body"]
```

By looking at the channel value you can identity the type of event :

NEW_CONTEST -> To announce new available contests
NEW_WINNER -> To announce when winners are available

###Log out

To clear all user informations associated with MSContest when user is logged out, call unlinkUser:

```kotlin
ContestSDK.unlinkUser()
```