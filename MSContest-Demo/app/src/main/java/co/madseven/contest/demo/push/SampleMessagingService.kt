package co.madseven.contest.demo.push

import android.util.Log
import co.madseven.contest.demo.App
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class SampleMessagingService : FirebaseMessagingService() {

    private val pushRepository: PushRepository by lazy {
        App.components.pushRepository
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.i("Push Test", remoteMessage.data.toString())
        val channel = remoteMessage.data["channel"]
        val title = remoteMessage.data["title"]
        val body = remoteMessage.data["body"]

        if (channel == "NEW_CONTEST") {
            //handle the notification the way you want for new contest
        }

        if (channel == "NEW_WINNER") {
            //handle the notification the way you want for new winner
        }
    }

    override fun onNewToken(token: String) {
        Log.i("onNewToken", "MessagingService : Refreshed token: $token")
        token.let {
            pushRepository.refreshToken()
        }
    }
}