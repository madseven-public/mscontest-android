package co.madseven.contest.demo.di

import android.content.Context
import co.madseven.contest.demo.push.PushRepository


class AppModule(override val applicationContext: Context) : AppComponent {

    override val pushRepository: PushRepository by lazy {
        PushRepository.Impl(applicationContext)
    }

}