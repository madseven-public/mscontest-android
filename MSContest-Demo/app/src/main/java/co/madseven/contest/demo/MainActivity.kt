package co.madseven.contest.demo

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import co.madseven.sdk.contest.ContestSDK

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.contestAccessActivity).setOnClickListener {
            ContestSDK.showContestListScreen(this)
        }

        findViewById<Button>(R.id.contestAccessFragment).setOnClickListener {
            val newIntent = Intent(this, BottomNavActivity::class.java)
            startActivity(newIntent)
        }

        findViewById<Button>(R.id.unlinkUser).setOnClickListener {
            ContestSDK.unlinkUser()
        }
    }
}