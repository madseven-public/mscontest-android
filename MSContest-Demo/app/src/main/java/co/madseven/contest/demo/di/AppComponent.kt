package co.madseven.contest.demo.di

import android.content.Context
import co.madseven.contest.demo.push.PushRepository

interface AppComponent {

    val applicationContext: Context
    val pushRepository: PushRepository

}