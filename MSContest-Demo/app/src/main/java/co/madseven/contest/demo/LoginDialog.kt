package co.madseven.contest.demo

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import co.madseven.contest.demo.databinding.DialogLoginBinding
import co.madseven.sdk.contest.ContestSDK

class LoginDialog : DialogFragment() {

    companion object {
        const val TAG: String = "CodeDialog"

        fun newInstance(): LoginDialog =
            LoginDialog()
    }

    private var binding: DialogLoginBinding? = null

    //region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogLoginBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        displayContent()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        return dialog
    }

    fun start(fragmentManager: FragmentManager) {
        show(
            fragmentManager,
            TAG
        )
    }

    //region State


    private fun displayContent() {
        binding?.apply {
            popupOkButton.setOnClickListener {
                //Here call your connect solution and for success callback link user to ContestSDK
                onUserConnectedCallback(
                    "42",
                    loginUserName.text.toString(),
                    loginMail.text.toString()
                )
            }
            popupCancelButton.setOnClickListener {
                dismiss()
            }
        }
    }


    private fun onUserConnectedCallback(yourUserId: String, username: String, mail: String) {
        ContestSDK.linkUser(yourUserId, username, mail)
        dismiss()
    }

    //endregion

}