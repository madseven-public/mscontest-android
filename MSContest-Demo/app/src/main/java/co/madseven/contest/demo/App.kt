package co.madseven.contest.demo

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.LifecycleObserver
import co.madseven.contest.demo.di.AppComponent
import co.madseven.contest.demo.di.AppModule
import co.madseven.sdk.contest.ContestSDK
import co.madseven.sdk.contest.UserNotConnectedCallback
import co.madseven.sdk.contest.ui.config.ContestThemeConfig
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class App : Application(), LifecycleObserver, ContestSDK.Builder.ContestSDKInitListener {

    companion object {
        lateinit var components: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        components = AppModule(this)

        GlobalScope.launch {
            components.pushRepository.refreshToken()
        }

        val themeConfig =
            ContestThemeConfig.Builder()
                .primaryColor(R.color.appMainColor)
                .backgroundColor(R.color.appBackgroundColor)
                .canParticipateFromList(true)
                .displayTimerFromDay(2)
                .mediumFontRes(R.font.montserrat_medium)
                .regularFontRes(R.font.montserrat_regular)
                .lightFontRes(R.font.montserrat_light)
                .shouldDisplaySponsorInList(true)
                .build()

        useMSContestLoginBuild(themeConfig)
        //useMSContestLoginBuild(themeConfig)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    private fun useCustomLoginBuilder(themeConfig: ContestThemeConfig) {

        val callback = object : UserNotConnectedCallback {
            override fun onUserNotConnected(activity: Activity) {
                val loginDialog = LoginDialog.newInstance()
                loginDialog.start((activity as AppCompatActivity).supportFragmentManager)
            }
        }

        ContestSDK.Builder(this, this)
            .apiToken("8LTwzvmufbc3APHKZbhZL2TDP5R3vPfH")
            .clientId("53636625")
            .linkAppUser(true)
            .userNotConnectedCallback(callback)
            .themeConfig(themeConfig)
            .debugMode(true)
            .build()
    }

    private fun useMSContestLoginBuild(themeConfig: ContestThemeConfig) {
        ContestSDK.Builder(this, this)
            .apiToken("8LTwzvmufbc3APHKZbhZL2TDP5R3vPfH")
            .clientId("53636625")
            .debugMode(true)
            .themeConfig(themeConfig)
            .build()
    }

    override fun onSDKInitialized() {
        Log.i("ContestSDKInit", "SDK is initialized")
        ContestSDK.linkUser(
            userId = "",
            username = "User Test",
            mail = "user_test@fakemail.com"
        )
    }

    override fun onSDKInitFailed(error: String) {
        Log.i("ContestSDKInit", "SDK failes with error : $error")
    }
}