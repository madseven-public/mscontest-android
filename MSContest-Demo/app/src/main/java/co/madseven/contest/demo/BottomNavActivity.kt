package co.madseven.contest.demo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import co.madseven.sdk.contest.ui.view.list.ContestListFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class BottomNavActivity : AppCompatActivity() {

    private val contestListFragment = ContestListFragment.newInstance()
    private val dummyFragment = DummyFragment.newInstance()

    //region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_nav)


        findViewById<BottomNavigationView>(R.id.bottomNavigationBar).setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_dummy -> setStateDummy()
                R.id.menu_contest -> setStateContestList()
                else -> setStateDummy()
            }
            true
        }

        initFragments()
    }

    //endregion

    //region States

    private fun setStateContestList() {
        supportFragmentManager.beginTransaction()
            .show(contestListFragment)
            .hide(dummyFragment)
            .commit()
    }

    private fun setStateDummy() {
        supportFragmentManager.beginTransaction()
            .hide(contestListFragment)
            .show(dummyFragment)
            .commit()
    }

    //endregion

    //region Navigation

    private fun initFragments() {
        supportFragmentManager.fragments.forEach { fragment ->
            fragment?.let {
                supportFragmentManager.beginTransaction().remove(it).commit()
            }
        }

        supportFragmentManager.beginTransaction()
            .addDummyFragment()
            .addContestListFragment()
            .hide(contestListFragment)
            .commit()
    }

    private fun FragmentTransaction.addDummyFragment(): FragmentTransaction {
        if (supportFragmentManager.findFragmentByTag(DummyFragment.TAG) == null) {
            add(R.id.contentFrameLayout, dummyFragment, DummyFragment.TAG)
        }
        return this
    }

    private fun FragmentTransaction.addContestListFragment(): FragmentTransaction {
        if (supportFragmentManager.findFragmentByTag("YOUR_TAG") == null) {
            add(R.id.contentFrameLayout, contestListFragment, "YOUR_TAG")
        }
        return this
    }

    //endregion
}