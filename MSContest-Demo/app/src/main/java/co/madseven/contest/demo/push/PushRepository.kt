package co.madseven.contest.demo.push

import android.content.Context
import co.madseven.sdk.contest.ContestSDK
import com.google.firebase.messaging.FirebaseMessaging

interface PushRepository {

    companion object {

        const val TOKEN_PREFS = "WJBFMUZUNYYPZGJBVHKDEZLMCBPNLFLQ"
        const val PREF_KEY_FIREBASE_MESSAGING_TOKEN = "XGFFJUSGSMHEAMJUCLZBNXJDKFYTOJGC"
    }

    fun refreshToken()
    fun getTokenSaved(): String

    class Impl(private val applicationContext: Context) : PushRepository {

        override fun refreshToken() {
            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    task.result?.let { token ->
                        ContestSDK.linkPushToken(token)
                        saveToken(token)
                    }
                }
            }
        }

        override fun getTokenSaved(): String {
            return applicationContext.getSharedPreferences(TOKEN_PREFS, Context.MODE_PRIVATE)
                .getString(PREF_KEY_FIREBASE_MESSAGING_TOKEN, "") ?: ""
        }

        private fun saveToken(token: String) {
            val preferences =
                applicationContext.getSharedPreferences(TOKEN_PREFS, Context.MODE_PRIVATE)
            preferences.edit().putString(PREF_KEY_FIREBASE_MESSAGING_TOKEN, token).apply()
        }
    }
}

